﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using System.IO;

namespace PatreonPauling
{
	class PatreonConfig
	{
		private const string _configFile = "PatreonConfig.json";
		private const string _configFolder = "Resources";

		public Config Patreon;

		public PatreonConfig()
		{

			if (!Directory.Exists(_configFolder)) Directory.CreateDirectory(_configFolder);
			if (!File.Exists(_configFolder + "/" + _configFile))
			{
				Patreon = new Config();
				string json = JsonConvert.SerializeObject(Patreon, Formatting.Indented);
				File.WriteAllText(_configFolder + "/" + _configFile, json);
			}
			else
			{
				string json = File.ReadAllText(_configFolder + "/" + _configFile);
				Patreon = JsonConvert.DeserializeObject<Config>(json);

			}
		}

		public void ModifyConfig(Patreon.Components.PatreonRefreshResponse response)
		{
			Config oldConfig = Program._PatreonConfig.Patreon;
			Config config = new Config();
			config.AccessToken = response.AccessToken;
			config.RefreshToken = response.RefreshToken;
			config.APIVersion = oldConfig.APIVersion;
			config.AppName = oldConfig.AppName;
			config.ClientID = oldConfig.ClientID;
			config.ClientSecret = oldConfig.ClientSecret;
			config.Roles = oldConfig.Roles;
			Program._PatreonConfig.Patreon = config;
			File.WriteAllText(_configFolder + "/" + _configFile, JsonConvert.SerializeObject(config, Formatting.Indented));
		}

		public struct Config
		{
			public string AppName;
			public string ClientID;
			public int APIVersion;
			public string ClientSecret;
			public string AccessToken;
			public string RefreshToken;
			public string OAuthCode;
			public List<PatronLevel> Roles;
		}

		public struct PatronLevel
		{
			public string DiscordRoleName;
			public int UpperPayBoundInCents;
			public int LowerPayBoundInCents;

			public PatronLevel(string name, int top, int bottom)
			{
				DiscordRoleName = name;
				UpperPayBoundInCents = top;
				LowerPayBoundInCents = bottom;
			}
		}
	}
}