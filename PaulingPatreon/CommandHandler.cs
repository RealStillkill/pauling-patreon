﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;

using System;
using System.Reflection;
using System.Threading.Tasks;

namespace PatreonPauling
{
	public class CommandHandlingService
	{
		private readonly CommandService _commands;
		private readonly DiscordSocketClient _client;
		private readonly IServiceProvider _provider;

		public CommandHandlingService(IServiceProvider provider, DiscordSocketClient client, CommandService commands)
		{
			_commands = commands;
			_client = client;
			_provider = provider;

			_client.MessageReceived += OnMessageReceived;
		}

		private async Task OnMessageReceived(SocketMessage m)
		{
			if (!(m is SocketUserMessage message)) return;
			if (message.Source != MessageSource.User) return;
			if (message.Author.IsBot) return;

			int pos = 0;
			if (!(message.HasStringPrefix(Program.Config.bot.cmdPrefix, ref pos)
				|| message.HasMentionPrefix(_client.CurrentUser, ref pos))) return;

			var context = new SocketCommandContext(_client, message);

			var result = await _commands.ExecuteAsync(context, pos, _provider);
			if (!result.IsSuccess)
			{
				await context.Channel.SendMessageAsync(string.Format("{0}: {1}", message.Author.Mention, result.ErrorReason));
				await context.Channel.SendMessageAsync("If this issue reoccures, please create a ticket here: \nhttps://bitbucket.org/RealStillkill/paulingpatreon/issues/new");
			}

		}

		public Task InitializeAsync()
			=> _commands.AddModulesAsync(Assembly.GetEntryAssembly(), _provider);
	}
}
