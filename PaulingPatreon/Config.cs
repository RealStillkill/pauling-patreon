﻿using System.IO;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace PatreonPauling
{
	class Config
	{
		private const string _configFolder = "Resources";
		private const string _configFile = "Config.json";

		public BotConfig bot;
		public Config()
		{

			if (!Directory.Exists(_configFolder)) Directory.CreateDirectory(_configFolder);
			if (!File.Exists(_configFolder + "/" + _configFile))
			{
				bot = new BotConfig();
				string json = JsonConvert.SerializeObject(bot, Formatting.Indented);
				File.WriteAllText(_configFolder + "/" + _configFile, json);
			}
			else
			{
				string json = File.ReadAllText(_configFolder + "/" + _configFile);
				bot = JsonConvert.DeserializeObject<BotConfig>(json);

			}
		}

		internal class BotConfig
		{
			[JsonConstructor]
			public BotConfig()
			{
				token = string.Empty;
				cmdPrefix = "//";
			}

			public string token;
			public string cmdPrefix;
		}
	}
}