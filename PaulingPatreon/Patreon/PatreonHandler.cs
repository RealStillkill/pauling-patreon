﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Timers;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;
using PatreonPauling.Patreon.Components;
using PaulingPatreon.Patreon.Components;
using System.Linq;
using Discord;
using PatreonPauling.Patreon;
using PatreonPauling;

namespace PaulingPatreon.Patreon
{
	class PatreonHandler
	{
		private OAuth _PatreonAuth;
		private PatreonAPI API;
		private Dictionary<string, object> _Tokens;
		public PatreonHandler()
		{
			this._PatreonAuth = new OAuth(Program._PatreonConfig.Patreon.ClientID, Program._PatreonConfig.Patreon.ClientSecret);
			this._Tokens = _PatreonAuth.GetTokens(Program._PatreonConfig.Patreon.OAuthCode, "https://bitbucket.org/RealStillkill/discordpatreonmanager/src/master/");
			this.API = new PatreonAPI(Program._PatreonConfig.Patreon.AccessToken);
		}

		public Patrons GetPatrons()
		{
			object response = API.FetchCampaignAndPatrons();
			Patrons ptr = Patrons.FromJson(response.ToString());
			string sr1 = JsonConvert.SerializeObject(ptr, Formatting.Indented); //Makes it so that the return is indented and readable for debug. Fuck your performance hit
			File.WriteAllText("GetPatrons.json", sr1);
			return ptr;
		}

		public PatreonPledgeResponse GetPledges()
		{
			//write to file to debug api response
			//File.WriteAllText("GetPledges.json", API.FetchPledges());
			PatreonPledgeResponse response = PatreonPledgeResponse.FromJson(API.FetchPledges());
			//Write to file for debug
			//File.WriteAllText("GetPledges.json", JsonConvert.SerializeObject(response, Formatting.Indented));
			return response;
		}

		public async void RefreshToken()
		{
			await API.FetchRefreshToken();
		}

		public struct pledge
		{
			public string PatreonName;
			public ulong DiscordID;
			public int DonationID;
			public pledge(string pname, ulong id, int donationId)
			{
				this.PatreonName = pname;
				this.DiscordID = id;
				this.DonationID = donationId;
			}
		}
	}
}