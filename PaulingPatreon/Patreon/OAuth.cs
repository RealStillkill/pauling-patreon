﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Net.Http;

namespace PaulingPatreon.Patreon
{
	class OAuth
	{
		private string clientId;
		private string clientSecret;
		private HttpClient oAuthClient = new HttpClient() { BaseAddress = new Uri("https://api.patreon.com/") };

		public OAuth(string clientId, string clientSecret)
		{
			this.clientId = clientId;
			this.clientSecret = clientSecret;
			oAuthClient.DefaultRequestHeaders.Add("User-Agent", "PatreonSharp (" + System.Runtime.InteropServices.RuntimeInformation.OSDescription + "," + System.Runtime.InteropServices.RuntimeInformation.OSArchitecture + ")");
		}

		public Dictionary<string, object> GetTokens(String code, String redirectUri)
		{
			return this.UpdateToken(new Dictionary<string, string> {
				{ "grant_type", "authorization_code" },
				{ "code", code },
				{ "client_id", clientId },
				{ "client_secret", clientSecret},
				{"redirect_uri", redirectUri }
			});
		}

		public Dictionary<string, object> RefreshToken(String refreshToken, String redirectUri)
		{
			return this.UpdateToken(new Dictionary<string, string> {
				{ "grant_type", "refresh_token" },
				{ "refresh_token", refreshToken },
				{ "client_id", clientId },
				{ "client_secret", clientSecret},
			});
		}

		private Dictionary<string, object> UpdateToken(Dictionary<string, string> param)
		{
			FormUrlEncodedContent formParameters = new FormUrlEncodedContent(param);
			HttpRequestMessage request = new HttpRequestMessage
			{
				RequestUri = new Uri("oauth2/token", UriKind.Relative),
				Method = HttpMethod.Post,
				Content = formParameters
			};

			var response = oAuthClient.SendAsync(request);

			using (HttpContent content = response.Result.Content)
			{
				string data = content.ReadAsStringAsync().Result;
				return JsonConvert.DeserializeObject<Dictionary<string, object>>(data);
			}
		}
	}
}
