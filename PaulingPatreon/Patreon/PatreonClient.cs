﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Http;
using Newtonsoft.Json;
using System.IO;
using System.Threading.Tasks;

namespace PaulingPatreon.Patreon
{
	[Obsolete("This is obsolete. Not that it ever worked. Use PatreonHandler", true)]
	class PatreonClient
	{
		//Creator's access token: 05BJWgOvc78RyG5coLvzrD4AReox3xVi20JJ_SqYsqA
		//Code = wyuwa8JfzoqE5Xx55d4AmMrwFj5KFX
		string CampaignID = "2407361";
		private readonly string oauthlink = "https://www.patreon.com/oauth2/authorize?response_type=code&client_id=wG3KujDhy8FWWNg9dCBSmnsvHww01cljeq4T24NI7XxG3uq548YcEuX4J4SwAqGq&redirect_uri=https://GamesNFriends.com";
		private PatreonInfo _Info;
		public PatreonClient()
		{




			//HttpWebRequest Request = HttpWebRequest.CreateHttp(oauthlink);
			////Request.AllowAutoRedirect = false;

			//string location;
			//using (var response = Request.GetResponse() as HttpWebResponse)
			//{
			//	location = response.GetResponseHeader("Location");
			//	Console.WriteLine(response.ToString());
			//}

			//Console.WriteLine(location);
			//Console.ReadKey();
		}

public async Task GetOAuth()
{
	using (var client = new HttpClient(new HttpClientHandler() { AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip }))
	{
		var request = new HttpRequestMessage()
		{
			RequestUri = new Uri(oauthlink),
			Method = HttpMethod.Get
		};

		HttpResponseMessage response = client.SendAsync(request).Result;
		int statusCode = (int)response.StatusCode;
		if (statusCode >= 300 && statusCode <= 399)
		{
			var redirectUri = response.Headers.Location;
			if (!redirectUri.IsAbsoluteUri)
			{
				redirectUri = new Uri(request.RequestUri.GetLeftPart(UriPartial.Authority) + redirectUri);
			}
			Console.WriteLine(redirectUri.OriginalString);
			Console.ReadKey();
		}
		else if (statusCode == 200)
		{
			Console.WriteLine(response.Content.ToString());
		}
		else if (!response.IsSuccessStatusCode)
		{
			throw new Exception();
		}
	}
}
		public void GetPledges()
		{
			//WebRequest webReq = WebRequest.Create($"https://www.patreon.com/api/oauth2/api/campaigns/2407361/pledges?include=patron.null");
			//webReq.Headers.Add("authorization: Bearer 05BJWgOvc78RyG5coLvzrD4AReox3xVi20JJ_SqYsqA");
			//WebResponse webResp = webReq.GetResponse();
			//File.WriteAllText("Patreon.json", webResp.ToString());

			//using (var client = new HttpClient(new HttpClientHandler() { AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip }))
			using (var client = new HttpClient(new HttpClientHandler()))
			{
				var request = new HttpRequestMessage()
				{
					RequestUri = new Uri("https://www.patreon.com/api/oauth2/api/campaigns/2407361/pledges?include=patron.null"),
					Method = HttpMethod.Get
				};
				request.Headers.Add("Authorization", "Bearer 05BJWgOvc78RyG5coLvzrD4AReox3xVi20JJ_SqYsqA");
				HttpResponseMessage response = client.SendAsync(request).Result;
				int statusCode = (int)response.StatusCode;
				if (statusCode >= 300 && statusCode <= 399)
				{
					var redirectUri = response.Headers.Location;
					if (!redirectUri.IsAbsoluteUri)
					{
						redirectUri = new Uri(request.RequestUri.GetLeftPart(UriPartial.Authority) + redirectUri);
					}
					Console.WriteLine(redirectUri.OriginalString);
					Console.ReadKey();
				}
				else if (statusCode == 200)
				{
					Console.WriteLine(response.Content.ToString());
				}
				else if (!response.IsSuccessStatusCode)
				{
					throw new Exception();
				}
			}
		}
	}

	public class PatreonInfo
	{
		[JsonProperty("access_token")]
		public string AccessToken { get; set; }

		[JsonProperty("refresh_token")]
		public string RefreshToken { get; set; }

		[JsonProperty("expires_in")]
		public string ExpiresIn { get; set; }

		[JsonProperty("scope")]
		public string Scope { get; set; }

		[JsonProperty("token_type")]
		public string TokenType { get; set; }
	}
}
