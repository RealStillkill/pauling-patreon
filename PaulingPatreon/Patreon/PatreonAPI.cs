﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Net.Http;
using System.Web;
using System.Threading.Tasks;

namespace PatreonPauling.Patreon
{
	class PatreonAPI
	{
		enum ReturnFormat
		{
			ApiReturnFormatJson = 0,
			ApiReturnFormatDictionary = 1
		}
		private String accessToken;
		private HttpClient patreonClient = new HttpClient() { BaseAddress = new Uri("https://api.patreon.com/oauth2/api/") };
		private ReturnFormat returnFormatType;

		public PatreonAPI(string accessToken)
		{
			this.accessToken = accessToken;
			patreonClient.DefaultRequestHeaders.Add("User-Agent", "PatreonSharp (" + System.Runtime.InteropServices.RuntimeInformation.OSDescription + "," + System.Runtime.InteropServices.RuntimeInformation.OSArchitecture + ")");
			patreonClient.DefaultRequestHeaders.Add("Authorization", "Bearer " + accessToken);
		}

		public object FetchUser()
		{
			return GetData("current_user");
		}

		public object FetchCampaignAndPatrons()
		{
			return GetData("current_user/campaigns?include=rewards,creator,goals,pledges").Result;
		}

		public object FetchCampaign()
		{
			return GetData("current_user/campaigns?include=rewards,creator,goals");
		}

		public string FetchPledges()
		{
			return GetData("campaigns/2407361/pledges?include=patron.null").Result.ToString();
		}

		public object FetchPageofPledges(long campaignId, int pageSize, string cursor = null)
		{
			String url = "campaigns/" + campaignId.ToString() + "/pledges?page%5Bcount%5D=" + pageSize.ToString();

			if (!string.IsNullOrEmpty(cursor))
			{
				string escapedCursor = HttpUtility.UrlEncode(cursor);
				url = url + "&page%5Bcursor%5D=" + escapedCursor;
			}

			var response = GetData(url).Result;
			Console.WriteLine(response);

			return GetData(url);
		}

		public async Task FetchRefreshToken()
		{
			HttpRequestMessage request = new HttpRequestMessage
			{//$"/token?grant_type=refresh_token&refresh_token={Program._PatreonConfig.Patreon.RefreshToken}&client_id={Program._PatreonConfig.Patreon.ClientID}&client_secret={Program._PatreonConfig.Patreon.ClientSecret}"
				RequestUri = new Uri($"/api/oauth2/token?grant_type=refresh_token&refresh_token={Program._PatreonConfig.Patreon.RefreshToken}&client_id={Program._PatreonConfig.Patreon.ClientID}&client_secret={Program._PatreonConfig.Patreon.ClientSecret}", UriKind.RelativeOrAbsolute),
				Method = HttpMethod.Post
			};
			HttpClient client = new HttpClient() { BaseAddress = new Uri("https://www.patreon.com") };
			client.DefaultRequestHeaders.Add("User-Agent", "PatreonSharp (" + System.Runtime.InteropServices.RuntimeInformation.OSDescription + "," + System.Runtime.InteropServices.RuntimeInformation.OSArchitecture + ")");
			client.DefaultRequestHeaders.Add("Authorization", "Bearer " + accessToken);
			var response = await client.SendAsync(request);
			//var response = patreonClient.SendAsync(request);
			await System.IO.File.WriteAllTextAsync("refresh.txt", await response.Content.ReadAsStringAsync());
			Program._PatreonConfig.ModifyConfig(JsonConvert.DeserializeObject<Components.PatreonRefreshResponse>(await response.Content.ReadAsStringAsync()));
			
		}
		public async Task<object> GetData(String suffix)
		{
			HttpRequestMessage request = new HttpRequestMessage
			{
				RequestUri = new Uri(suffix, UriKind.Relative),
				Method = HttpMethod.Get
			};

			var response = patreonClient.SendAsync(request);

			if (response.Result.StatusCode.GetHashCode() >= 500)
			{
				return response.Result.Content.ReadAsStringAsync().Result;
			}

			switch (returnFormatType)
			{
				case ReturnFormat.ApiReturnFormatJson:
				default:
					System.IO.File.WriteAllText("return.txt", response.Result.Content.ReadAsStringAsync().Result);
					return response.Result.Content.ReadAsStringAsync().Result;
				case ReturnFormat.ApiReturnFormatDictionary:
					return JsonConvert.DeserializeObject<Dictionary<string, object>>(response.Result.Content.ReadAsStringAsync().Result);
			}
		}
	}
}