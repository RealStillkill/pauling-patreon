﻿using System;
using Newtonsoft.Json;

namespace PatreonPauling.Patreon.Components
{
	public partial class PatreonPledgeResponse
	{
		[JsonProperty("data")]
		public Datum[] Data { get; set; }

		[JsonProperty("included")]
		public Included[] Included { get; set; }

		[JsonProperty("links")]
		public PatreonPledgeResponseLinks Links { get; set; }

		[JsonProperty("meta")]
		public Meta Meta { get; set; }
	}

	public partial class PledgeDatum
	{
		[JsonProperty("attributes")]
		public DatumAttributes Attributes { get; set; }

		[JsonProperty("id")]
		[JsonConverter(typeof(ParseStringConverter))]
		public long Id { get; set; }

		[JsonProperty("relationships")]
		public Relationships Relationships { get; set; }

		[JsonProperty("type")]
		public string Type { get; set; }
	}

	public partial class PledgeDatumAttributes
	{
		[JsonProperty("amount_cents")]
		public long AmountCents { get; set; }

		[JsonProperty("created_at")]
		public DateTimeOffset CreatedAt { get; set; }

		[JsonProperty("declined_since")]
		public object DeclinedSince { get; set; }

		[JsonProperty("patron_pays_fees")]
		public bool PatronPaysFees { get; set; }

		[JsonProperty("pledge_cap_cents")]
		public long PledgeCapCents { get; set; }
	}

	public partial class PledgeRelationships
	{
		[JsonProperty("patron")]
		public Patron Patron { get; set; }
	}

	public partial class PledgePatron
	{
		[JsonProperty("data")]
		public Data Data { get; set; }

		[JsonProperty("links")]
		public PatronLinks Links { get; set; }
	}

	public partial class PledgeData
	{
		[JsonProperty("id")]
		[JsonConverter(typeof(ParseStringConverter))]
		public long Id { get; set; }

		[JsonProperty("type")]
		public string Type { get; set; }
	}

	public partial class PledgePatronLinks
	{
		[JsonProperty("related")]
		public Uri Related { get; set; }
	}

	public partial class PledgeIncluded
	{
		[JsonProperty("attributes")]
		public IncludedAttributes Attributes { get; set; }

		[JsonProperty("id")]
		[JsonConverter(typeof(ParseStringConverter))]
		public long Id { get; set; }

		[JsonProperty("type")]
		public string Type { get; set; }
	}

	public partial class PledgeIncludedAttributes
	{
		[JsonProperty("about")]
		public string About { get; set; }

		[JsonProperty("created")]
		public DateTimeOffset Created { get; set; }

		[JsonProperty("default_country_code")]
		public string DefaultCountryCode { get; set; }

		[JsonProperty("email")]
		public string Email { get; set; }

		[JsonProperty("facebook")]
		public object Facebook { get; set; }

		[JsonProperty("first_name")]
		public string FirstName { get; set; }

		[JsonProperty("full_name")]
		public string FullName { get; set; }

		[JsonProperty("gender")]
		public long Gender { get; set; }

		[JsonProperty("image_url")]
		public Uri ImageUrl { get; set; }

		[JsonProperty("is_email_verified")]
		public bool IsEmailVerified { get; set; }

		[JsonProperty("last_name")]
		public string LastName { get; set; }

		[JsonProperty("social_connections")]
		public SocialConnections SocialConnections { get; set; }

		[JsonProperty("thumb_url")]
		public Uri ThumbUrl { get; set; }

		[JsonProperty("twitch")]
		public Uri Twitch { get; set; }

		[JsonProperty("twitter")]
		public string Twitter { get; set; }

		[JsonProperty("url")]
		public Uri Url { get; set; }

		[JsonProperty("vanity")]
		public string Vanity { get; set; }

		[JsonProperty("youtube")]
		public Uri Youtube { get; set; }
	}

	public partial class PledgeSocialConnections
	{
		[JsonProperty("deviantart")]
		public object Deviantart { get; set; }

		[JsonProperty("discord")]
		public Discord Discord { get; set; }

		[JsonProperty("facebook")]
		public object Facebook { get; set; }

		[JsonProperty("instagram")]
		public object Instagram { get; set; }

		[JsonProperty("reddit")]
		public object Reddit { get; set; }

		[JsonProperty("spotify")]
		public object Spotify { get; set; }

		[JsonProperty("twitch")]
		public object Twitch { get; set; }

		[JsonProperty("twitter")]
		public object Twitter { get; set; }

		[JsonProperty("youtube")]
		public Discord Youtube { get; set; }
	}

	public partial class PledgeDiscord
	{
		[JsonProperty("url")]
		public Uri Url { get; set; }

		[JsonProperty("user_id")]
		public string UserId { get; set; }
	}

	public partial class PatreonPledgeResponseLinks
	{
		[JsonProperty("first")]
		public Uri First { get; set; }
	}

	public partial class PledgeMeta
	{
		[JsonProperty("count")]
		public long Count { get; set; }
	}

	public partial class PatreonPledgeResponse
	{
		public static PatreonPledgeResponse FromJson(string json) => JsonConvert.DeserializeObject<PatreonPledgeResponse>(json);
	}
}
