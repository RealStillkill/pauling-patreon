﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace PatreonPauling.Patreon.Components
{
	[Obsolete("This has been replaced with PatreonPledgeResponse", true)]
	public partial class PatronPledgeReturn
	{
		[JsonProperty("data")]
		public Datum[] Data { get; set; }

		[JsonProperty("included")]
		public Included[] Included { get; set; }

		[JsonProperty("links")]
		public PatronPledgeReturnLinks Links { get; set; }

		[JsonProperty("meta")]
		public Meta Meta { get; set; }
	}

	public partial class Datum
	{
		[JsonProperty("attributes")]
		public DatumAttributes Attributes { get; set; }

		[JsonProperty("id")]
		[JsonConverter(typeof(ParseStringConverter))]
		public long Id { get; set; }

		[JsonProperty("relationships")]
		public Relationships Relationships { get; set; }

		[JsonProperty("type")]
		public string Type { get; set; }
	}

	public partial class DatumAttributes
	{
		[JsonProperty("amount_cents")]
		public long AmountCents { get; set; }

		[JsonProperty("created_at")]
		public DateTimeOffset CreatedAt { get; set; }

		[JsonProperty("declined_since")]
		public object DeclinedSince { get; set; }

		[JsonProperty("patron_pays_fees")]
		public bool PatronPaysFees { get; set; }

		[JsonProperty("pledge_cap_cents")]
		public long PledgeCapCents { get; set; }
	}

	public partial class Relationships
	{
		[JsonProperty("patron")]
		public Patron Patron { get; set; }
	}

	public partial class Patron
	{
		[JsonProperty("data")]
		public Data Data { get; set; }

		[JsonProperty("links")]
		public PatronLinks Links { get; set; }
	}

	public partial class Data
	{
		[JsonProperty("id")]
		[JsonConverter(typeof(ParseStringConverter))]
		public long Id { get; set; }

		[JsonProperty("type")]
		public string Type { get; set; }
	}

	public partial class PatronLinks
	{
		[JsonProperty("related")]
		public Uri Related { get; set; }
	}

	public partial class Included
	{
		[JsonProperty("attributes")]
		public IncludedAttributes Attributes { get; set; }

		[JsonProperty("id")]
		[JsonConverter(typeof(ParseStringConverter))]
		public long Id { get; set; }

		[JsonProperty("type")]
		public string Type { get; set; }
	}

	public partial class IncludedAttributes
	{
		[JsonProperty("about")]
		public object About { get; set; }

		[JsonProperty("created")]
		public DateTimeOffset Created { get; set; }

		[JsonProperty("default_country_code")]
		public object DefaultCountryCode { get; set; }

		[JsonProperty("email")]
		public string Email { get; set; }

		[JsonProperty("facebook")]
		public object Facebook { get; set; }

		[JsonProperty("first_name")]
		public string FirstName { get; set; }

		[JsonProperty("full_name")]
		public string FullName { get; set; }

		[JsonProperty("gender")]
		public long Gender { get; set; }

		[JsonProperty("image_url")]
		public Uri ImageUrl { get; set; }

		[JsonProperty("is_email_verified")]
		public bool IsEmailVerified { get; set; }

		[JsonProperty("last_name")]
		public string LastName { get; set; }

		[JsonProperty("social_connections")]
		public SocialConnections SocialConnections { get; set; }

		[JsonProperty("thumb_url")]
		public Uri ThumbUrl { get; set; }

		[JsonProperty("twitch")]
		public object Twitch { get; set; }

		[JsonProperty("twitter")]
		public object Twitter { get; set; }

		[JsonProperty("url")]
		public Uri Url { get; set; }

		[JsonProperty("vanity")]
		public object Vanity { get; set; }

		[JsonProperty("youtube")]
		public object Youtube { get; set; }
	}

	public partial class SocialConnections
	{
		[JsonProperty("deviantart")]
		public object Deviantart { get; set; }

		[JsonProperty("discord")]
		public Discord Discord { get; set; }

		[JsonProperty("facebook")]
		public object Facebook { get; set; }

		[JsonProperty("instagram")]
		public object Instagram { get; set; }

		[JsonProperty("reddit")]
		public object Reddit { get; set; }

		[JsonProperty("spotify")]
		public object Spotify { get; set; }

		[JsonProperty("twitch")]
		public object Twitch { get; set; }

		[JsonProperty("twitter")]
		public object Twitter { get; set; }

		[JsonProperty("youtube")]
		public object Youtube { get; set; }
	}

	public partial class Discord
	{
		[JsonProperty("url")]
		public object Url { get; set; }

		[JsonProperty("user_id")]
		public string UserId { get; set; }
	}

	public partial class PatronPledgeReturnLinks
	{
		[JsonProperty("first")]
		public Uri First { get; set; }
	}

	public partial class Meta
	{
		[JsonProperty("count")]
		public long Count { get; set; }
	}

	public partial class PatronPledgeReturn
	{
		public static PatronPledgeReturn FromJson(string json) => JsonConvert.DeserializeObject<PatronPledgeReturn>(json, PatreonPauling.Patreon.Converter.Settings);
	}

	public static class Serialize
	{
		//public static string ToJson(this PatronPledgeReturn self) => JsonConvert.SerializeObject(self, Pauling.Patreon.Converter.Settings);
	}

	internal static class Converter
	{
		public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
		{
			MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
			DateParseHandling = DateParseHandling.None,
			Converters =
			{
				new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
			},
		};
	}

	internal class ParseStringConverter : JsonConverter
	{
		public override bool CanConvert(Type t) => t == typeof(long) || t == typeof(long?);

		public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
		{
			if (reader.TokenType == JsonToken.Null) return null;
			var value = serializer.Deserialize<string>(reader);
			long l;
			if (Int64.TryParse(value, out l))
			{
				return l;
			}
			throw new Exception("Cannot unmarshal type long");
		}

		public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
		{
			if (untypedValue == null)
			{
				serializer.Serialize(writer, null);
				return;
			}
			var value = (long)untypedValue;
			serializer.Serialize(writer, value.ToString());
			return;
		}

		public static readonly ParseStringConverter Singleton = new ParseStringConverter();
	}
}
