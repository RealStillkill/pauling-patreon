﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using PatreonPauling.Patreon;

namespace PaulingPatreon.Patreon.Components
{
	class PatreonUserData
	{
		public string Type { get; private set; }
		public string Id { get; private set; }

		internal class Attributes
		{
			public string FirstName { get; private set; }
			public string LastName { get; private set; }
			public string FullName { get; private set; }
			public string Vanity { get; private set; }
			public string Email { get; private set; }
			public string About { get; private set; }
			public string FacebookId { get; private set; }
			public string ImageURL { get; private set; }
			public string ThumbnailURL { get; private set; }
			public string Youtube { get; private set; }
			public string Facebook { get; private set; }
			public DateTime Created { get; private set; }
			public string Url { get; private set; }

			public Attributes(Patrons response)
			{
				//response.Data.
			}
		}
	}
}
