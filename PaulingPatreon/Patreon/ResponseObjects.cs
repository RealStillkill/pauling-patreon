﻿using System;
using System.Collections.Generic;

using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace PatreonPauling.Patreon
{
	public partial class Patrons
	{
		[JsonProperty("data")]
		public Datum[] Data { get; set; }

		[JsonProperty("included")]
		public Included[] Included { get; set; }
	}

	public partial class Datum
	{
		[JsonProperty("attributes")]
		public DatumAttributes Attributes { get; set; }

		[JsonProperty("id")]
		public string Id { get; set; }

		[JsonProperty("relationships")]
		public DatumRelationships Relationships { get; set; }

		[JsonProperty("type")]
		public string Type { get; set; }
	}

	public partial class DatumAttributes
	{
		[JsonProperty("created_at")]
		public DateTimeOffset CreatedAt { get; set; }

		[JsonProperty("creation_count")]
		public long CreationCount { get; set; }

		[JsonProperty("creation_name")]
		public string CreationName { get; set; }

		[JsonProperty("discord_server_id")]
		public object DiscordServerId { get; set; }

		[JsonProperty("display_patron_goals")]
		public bool DisplayPatronGoals { get; set; }

		[JsonProperty("earnings_visibility")]
		public string EarningsVisibility { get; set; }

		[JsonProperty("image_small_url")]
		public object ImageSmallUrl { get; set; }

		[JsonProperty("image_url")]
		public object ImageUrl { get; set; }

		[JsonProperty("is_charged_immediately")]
		public bool IsChargedImmediately { get; set; }

		[JsonProperty("is_monthly")]
		public bool IsMonthly { get; set; }

		[JsonProperty("is_nsfw")]
		public bool IsNsfw { get; set; }

		[JsonProperty("is_plural")]
		public bool IsPlural { get; set; }

		[JsonProperty("main_video_embed")]
		public object MainVideoEmbed { get; set; }

		[JsonProperty("main_video_url")]
		public object MainVideoUrl { get; set; }

		[JsonProperty("one_liner")]
		public object OneLiner { get; set; }

		[JsonProperty("outstanding_payment_amount_cents")]
		public long OutstandingPaymentAmountCents { get; set; }

		[JsonProperty("patron_count")]
		public long PatronCount { get; set; }

		[JsonProperty("pay_per_name")]
		public object PayPerName { get; set; }

		[JsonProperty("pledge_sum")]
		public long PledgeSum { get; set; }

		[JsonProperty("pledge_url")]
		public string PledgeUrl { get; set; }

		[JsonProperty("published_at")]
		public DateTimeOffset PublishedAt { get; set; }

		[JsonProperty("summary")]
		public object Summary { get; set; }

		[JsonProperty("thanks_embed")]
		public object ThanksEmbed { get; set; }

		[JsonProperty("thanks_msg")]
		public object ThanksMsg { get; set; }

		[JsonProperty("thanks_video_url")]
		public object ThanksVideoUrl { get; set; }
	}

	public partial class DatumRelationships
	{
		[JsonProperty("creator")]
		public Creator Creator { get; set; }

		[JsonProperty("goals")]
		public Goals Goals { get; set; }

		[JsonProperty("rewards")]
		public Goals Rewards { get; set; }
	}

	public partial class Creator
	{
		[JsonProperty("data")]
		public Dat Data { get; set; }

		[JsonProperty("links")]
		public Links Links { get; set; }
	}

	public partial class Dat
	{
		[JsonProperty("id")]
		public string Id { get; set; }

		[JsonProperty("type")]
		public string Type { get; set; }
	}

	public partial class Links
	{
		[JsonProperty("related")]
		public Uri Related { get; set; }
	}

	public partial class Goals
	{
		[JsonProperty("data")]
		public Dat[] Data { get; set; }
	}

	public partial class Included
	{
		[JsonProperty("attributes")]
		public IncludedAttributes Attributes { get; set; }

		[JsonProperty("id")]
		[JsonConverter(typeof(ParseStringConverter))]
		public long Id { get; set; }

		[JsonProperty("relationships")]
		public IncludedRelationships Relationships { get; set; }

		[JsonProperty("type")]
		public string Type { get; set; }
	}

	public partial class IncludedAttributes
	{
		[JsonProperty("about")]
		public object About { get; set; }

		[JsonProperty("created", NullValueHandling = NullValueHandling.Ignore)]
		public DateTimeOffset? Created { get; set; }

		[JsonProperty("discord_id")]
		public object DiscordId { get; set; }

		[JsonProperty("email", NullValueHandling = NullValueHandling.Ignore)]
		public string Email { get; set; }

		[JsonProperty("facebook")]
		public object Facebook { get; set; }

		[JsonProperty("facebook_id")]
		public object FacebookId { get; set; }

		[JsonProperty("first_name", NullValueHandling = NullValueHandling.Ignore)]
		public string FirstName { get; set; }

		[JsonProperty("full_name", NullValueHandling = NullValueHandling.Ignore)]
		public string FullName { get; set; }

		[JsonProperty("gender", NullValueHandling = NullValueHandling.Ignore)]
		public long? Gender { get; set; }

		[JsonProperty("has_password", NullValueHandling = NullValueHandling.Ignore)]
		public bool? HasPassword { get; set; }

		[JsonProperty("image_url", NullValueHandling = NullValueHandling.Ignore)]
		public Uri ImageUrl { get; set; }

		[JsonProperty("is_deleted", NullValueHandling = NullValueHandling.Ignore)]
		public bool? IsDeleted { get; set; }

		[JsonProperty("is_email_verified", NullValueHandling = NullValueHandling.Ignore)]
		public bool? IsEmailVerified { get; set; }

		[JsonProperty("is_nuked", NullValueHandling = NullValueHandling.Ignore)]
		public bool? IsNuked { get; set; }

		[JsonProperty("is_suspended", NullValueHandling = NullValueHandling.Ignore)]
		public bool? IsSuspended { get; set; }

		[JsonProperty("last_name", NullValueHandling = NullValueHandling.Ignore)]
		public string LastName { get; set; }

		[JsonProperty("social_connections", NullValueHandling = NullValueHandling.Ignore)]
		public SocialConnections SocialConnections { get; set; }

		[JsonProperty("thumb_url", NullValueHandling = NullValueHandling.Ignore)]
		public Uri ThumbUrl { get; set; }

		[JsonProperty("twitch")]
		public object Twitch { get; set; }

		[JsonProperty("twitter")]
		public object Twitter { get; set; }

		[JsonProperty("url")]
		public Uri Url { get; set; }

		[JsonProperty("vanity", NullValueHandling = NullValueHandling.Ignore)]
		public string Vanity { get; set; }

		[JsonProperty("youtube")]
		public object Youtube { get; set; }

		[JsonProperty("amount", NullValueHandling = NullValueHandling.Ignore)]
		public long? Amount { get; set; }

		[JsonProperty("amount_cents", NullValueHandling = NullValueHandling.Ignore)]
		public long? AmountCents { get; set; }

		[JsonProperty("created_at")]
		public object CreatedAt { get; set; }

		[JsonProperty("description", NullValueHandling = NullValueHandling.Ignore)]
		public string Description { get; set; }

		[JsonProperty("id", NullValueHandling = NullValueHandling.Ignore)]
		[JsonConverter(typeof(ParseStringConverter))]
		public long? Id { get; set; }

		[JsonProperty("remaining", NullValueHandling = NullValueHandling.Ignore)]
		public long? Remaining { get; set; }

		[JsonProperty("requires_shipping", NullValueHandling = NullValueHandling.Ignore)]
		public bool? RequiresShipping { get; set; }

		[JsonProperty("type", NullValueHandling = NullValueHandling.Ignore)]
		public string Type { get; set; }

		[JsonProperty("user_limit")]
		public object UserLimit { get; set; }
	}

	public partial class SocialConnections
	{
		[JsonProperty("deviantart")]
		public object Deviantart { get; set; }

		[JsonProperty("discord")]
		public object Discord { get; set; }

		[JsonProperty("facebook")]
		public object Facebook { get; set; }

		[JsonProperty("reddit")]
		public object Reddit { get; set; }

		[JsonProperty("spotify")]
		public object Spotify { get; set; }

		[JsonProperty("twitch")]
		public object Twitch { get; set; }

		[JsonProperty("twitter")]
		public object Twitter { get; set; }

		[JsonProperty("youtube")]
		public object Youtube { get; set; }
	}

	public partial class IncludedRelationships
	{
		[JsonProperty("campaign", NullValueHandling = NullValueHandling.Ignore)]
		public Creator Campaign { get; set; }

		[JsonProperty("creator", NullValueHandling = NullValueHandling.Ignore)]
		public Creator Creator { get; set; }
	}

	public partial class Patrons
	{
		public static Patrons FromJson(string json) => JsonConvert.DeserializeObject<Patrons>(json, PatreonPauling.Patreon.Converter.Settings);
	}

	public static class Serialize
	{
		public static string ToJson(this Patrons self) => JsonConvert.SerializeObject(self, PatreonPauling.Patreon.Converter.Settings);
	}

	internal static class Converter
	{
		public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
		{
			MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
			DateParseHandling = DateParseHandling.None,
			Converters =
			{
				new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
			},
		};
	}

	internal class ParseStringConverter : JsonConverter
	{
		public override bool CanConvert(Type t) => t == typeof(long) || t == typeof(long?);

		public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
		{
			if (reader.TokenType == JsonToken.Null) return null;
			var value = serializer.Deserialize<string>(reader);
			long l;
			if (Int64.TryParse(value, out l))
			{
				return l;
			}
			throw new Exception("Cannot unmarshal type long");
		}

		public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
		{
			if (untypedValue == null)
			{
				serializer.Serialize(writer, null);
				return;
			}
			var value = (long)untypedValue;
			serializer.Serialize(writer, value.ToString());
			return;
		}

		public static readonly ParseStringConverter Singleton = new ParseStringConverter();
	}
}
