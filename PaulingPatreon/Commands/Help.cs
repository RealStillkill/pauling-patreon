﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

using Discord;
using Discord.WebSocket;
using Discord.Commands;
using PaulingPatreon.Commands;

namespace PatreonPauling.Commands
{
	public class Help
		: ModuleBase<SocketCommandContext>
	{
		[Command("help")]
		public async Task ListCommands()
		{
			var generalList = new List<string>();
			var adminList = new List<string>();

			var asm = Assembly.GetExecutingAssembly();

			var results = from type in asm.GetTypes()
						  where typeof(ModuleBase<SocketCommandContext>).IsAssignableFrom(type)
						  select type;

			foreach (var t in results)
			{
				if (t.GetTypeInfo().Name.Equals("GeneralCommands"))
				{
					foreach (var info in t.GetMethods())
					{
						var command = info.GetCustomAttribute(typeof(CommandAttribute)) as CommandAttribute;
						var summary = info.GetCustomAttribute(typeof(SummaryAttribute)) as SummaryAttribute;
						if (command != null)
						{
							generalList.Add(string.Format("//{0}{1}", command.Text, summary != null ? " - " + summary.Text : ""));
						}

						var alias = info.GetCustomAttribute(typeof(AliasAttribute)) as AliasAttribute;
						if (alias != null)
						{
							foreach (var name in alias.Aliases)
							{
								generalList.Add(string.Format("//{0}{1}", name, summary != null ? " - " + summary.Text : ""));
							}
						}
					}
				}

				if (t.GetTypeInfo().Name.Equals("AdministrativeCommands"))
				{
					var user = Context.User as SocketGuildUser;

					foreach (var info in t.GetMethods())
					{
						var requirements = info.GetCustomAttributes(typeof(RequireUserRoleAttribute)) as RequireUserRoleAttribute[];
						foreach (var requirement in requirements)
						{
							foreach (var role in Context.Guild.Roles)
							{
								if (requirement.Role.Equals(role.Name) && user.Roles.Contains(role))
								{
									var command = info.GetCustomAttribute(typeof(CommandAttribute)) as CommandAttribute;
									var summary = info.GetCustomAttribute(typeof(SummaryAttribute)) as SummaryAttribute;
									if (command != null)
									{
										adminList.Add(string.Format("//{0}{1}", command.Text, summary != null ? " - " + summary.Text : ""));
									}

									var alias = info.GetCustomAttribute(typeof(AliasAttribute)) as AliasAttribute;
									if (alias != null)
									{
										foreach (var name in alias.Aliases)
										{
											adminList.Add(string.Format("//{0}{1}", name, summary != null ? " - " + summary.Text : ""));
										}
									}
								}
							}
						}
					}
				}
			}

			if (adminList.Count == 0)
				adminList.Add("No access.");

			string general = "";
			string[] helplines = generalList.ToArray();
			foreach (string line in helplines)
			{
				general += line + "\n";
			}

			string administrative = "";
			helplines = adminList.ToArray();
			foreach (string line in helplines)
			{
				administrative += line + "\n";
			}

			var emb = new EmbedBuilder()
			{
				Title = "Command List",
				Color = Color.Red,
				Timestamp = DateTime.UtcNow
			};
			emb.AddField("General", general);
			emb.AddField("Administrative", administrative);

			await Context.Channel.SendMessageAsync("", false, emb.Build());
		}
	}
}
