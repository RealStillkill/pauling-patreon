﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Text.RegularExpressions;
using Discord;
using Discord.WebSocket;
using Discord.Commands;
using PatreonPauling.Patreon.Components;
using System.IO;
using Newtonsoft.Json;
using PatreonPauling;

namespace PaulingPatreon.Commands
{
	public class AdministrativeCommands : ModuleBase<SocketCommandContext>
	{
		[Command("CheckPatreon"), Summary("Applies relevant patreon role to specified user. Usage: //CheckPatreon <@mention>")]
		[RequireUserRole("Moderator")]
		public async Task CheckPatreon(IGuildUser user)
		{
			EmbedBuilder builder = new EmbedBuilder();
			builder.WithColor(Color.Teal);
			builder.WithTitle("Patreon Link");
			builder.WithDescription($"Link Target: {user.Username}#{user.Discriminator}({user.Nickname}<ulong:{user.Id}>");
			builder.AddField("Patreon User ID:", "Waiting for response...");
			builder.AddField("Pledge Level:", "Waiting for response...");
			builder.WithThumbnailUrl("https://i.imgur.com/ifV5WYA.gif");
			var msg = ReplyAsync("", false, builder.Build());
			builder.Fields.Clear();
			try
			{
				
				await Task.Delay(3000); //allow discord api gateway rest period
				bool isValisUser = false;
				var response = Program._PatreonHandler.GetPledges();
				Included patron;
				for (int i = 0; i < response.Included.Length; i++)
				{
					if (response.Included[i].Attributes.SocialConnections.Discord != null)
					{
						if (Convert.ToUInt64(response.Included[i].Attributes.SocialConnections.Discord.UserId) == user.Id)
						{
							isValisUser = true;
							patron = response.Included[i];

							builder.AddField("Patreon User ID:", patron.Id);
							foreach (var role in Program._PatreonConfig.Patreon.Roles)
							{
								//if payment is greaterthan or equal tp lowerbound and payment is less than or equal to upperbound
								if (response.Data[i].Attributes.AmountCents >= role.LowerPayBoundInCents && response.Data[i].Attributes.AmountCents <= role.UpperPayBoundInCents)
								{
									SocketRole socketRole = Context.Guild.Roles.FirstOrDefault(x => x.Name.ToString().ToLower() == role.DiscordRoleName);
									await user.AddRoleAsync(socketRole);
									builder.AddField("Pledge Level:", $"{role.DiscordRoleName}\n\nDiscord role applied!");
									builder.WithColor(Color.Green);
									builder.WithThumbnailUrl(user.GetAvatarUrl());
									builder.WithCurrentTimestamp();
								}
								//if upperbound is negative 1 (infinite, to cover for overpay) and payment is greater than or equal to lower bound
								if (role.UpperPayBoundInCents == -1)
								{
									if (response.Data[i].Attributes.AmountCents >= role.LowerPayBoundInCents)
									{
										SocketRole socketRole = Context.Guild.Roles.FirstOrDefault(x => x.Name.ToString().ToLower() == role.DiscordRoleName);
										await user.AddRoleAsync(socketRole);
										builder.AddField("Pledge Level:", $"{role.DiscordRoleName}\n\nDiscord role applied!");
										builder.WithColor(Color.Green);
										builder.WithThumbnailUrl(user.GetAvatarUrl());
										builder.WithCurrentTimestamp();
										await msg.Result.ModifyAsync(x =>
										{
											x.Content = "";
											x.Embed = builder.Build();
										});
									}
								}
							}
						}
						else
						{
							patron = null;
						}
					}
					else
					{
						patron = null;
					}
				}
				if (!isValisUser)
				{
					builder.AddField("Patron User ID:", "There are no patrons linked with this discord. Are your accounts linked?");
					builder.WithCurrentTimestamp();
					builder.WithThumbnailUrl(user.GetAvatarUrl());
					builder.WithColor(Color.Red);
					await msg.Result.ModifyAsync(x =>
					{
						x.Content = "";
						x.Embed = builder.Build();
					});
				}
			}
			catch (NullReferenceException e)
			{
				builder.AddField("Critical API Error:", "Patreon failed to return a valid response");
				builder.AddField("If this problem persists, please create a ticket at:", "https://bitbucket.org/RealStillkill/paulingpatreon/issues/new");
				builder.AddField("An exception has occurred:", e.StackTrace);
				builder.WithColor(Color.Red);
				builder.WithThumbnailUrl("https://discordapp.com/assets/b1868d829b37f0a81533ededb9ffe5f4.svg");
				builder.WithCurrentTimestamp();
				await msg.Result.ModifyAsync(x =>
				{
					x.Content = "";
					x.Embed = builder.Build();
				});
			}
			catch (Exception e)
			{
				builder.AddField("Critical Internal Error", "An internal exception has occurred");
				builder.AddField("If this problem persists, please create a ticket at:", "https://bitbucket.org/RealStillkill/paulingpatreon/issues/new");
				builder.AddField("An exception has occurred:", e.StackTrace);
				builder.WithColor(Color.Red);
				builder.WithThumbnailUrl("https://discordapp.com/assets/b1868d829b37f0a81533ededb9ffe5f4.svg");
				builder.WithCurrentTimestamp();
				await msg.Result.ModifyAsync(x =>
				{
					x.Content = "";
					x.Embed = builder.Build();
				});
			}


		}
		[Command("RefreshToken"), Summary("Requests updated API info")]
		[RequireUserRole("Moderator")]
		public async Task RefreshToken()
		{
			var msg = await ReplyAsync("New credentials requested.");
			try
			{
				Program._PatreonHandler.RefreshToken();
				await msg.ModifyAsync(x =>
				{
					x.Content = "New credentials saved to local storage.";
					x.Embed = null;
				});
			}
			catch (Exception e)
			{
				await msg.ModifyAsync(x =>
				{
					x.Content = "An error has occurred within the Patreon API.";
					x.Embed = null;
				});
			}
			
		}

	}

	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true, Inherited = true)]
	public class RequireUserRoleAttribute : PreconditionAttribute
	{
		public RequireUserRoleAttribute(string role) { Role = role; }

		public string Role { get; }

		public override Task<PreconditionResult> CheckPermissionsAsync(ICommandContext context, CommandInfo command, IServiceProvider services)
		{
			if(context is SocketCommandContext _context)
			{
				if(_context.User is SocketGuildUser _user)
				{
					foreach (SocketRole role in _context.Guild.Roles)
					{
						if (role.Name.Equals(Role) && _user.Roles.Contains(role))
							return Task.FromResult(PreconditionResult.FromSuccess());
					}
				}
			}
			return Task.FromResult(PreconditionResult.FromError("You don't have permissions to this command"));
		}
	}
}