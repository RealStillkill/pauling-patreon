﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
namespace PaulingPatreon.Commands
{
	public class GeneralCommands : ModuleBase<SocketCommandContext>
	{
		[Command("Role"), Summary("Applies relevant patreon role to specified user. (self if left blank) Usage: //CheckPatreon [@mention]")]
		public async Task CheckPatreon()
		{
			SocketGuildUser user = Context.Guild.GetUser(Context.User.Id);
			EmbedBuilder builder = new EmbedBuilder();
			builder.WithColor(Color.Teal);
			builder.WithTitle("Patreon Link");
			builder.WithDescription($"Link Target: {user.Username}#{user.Discriminator}({user.Nickname}<ulong:{user.Id}>");
			builder.AddField("Patreon User ID:", "Waiting for response...");
			builder.AddField("Pledge Level:", "Waiting for response...");
			builder.WithThumbnailUrl("https://i.imgur.com/ifV5WYA.gif");
			var msg = ReplyAsync("", false, builder.Build());
			try
			{
				
				builder.Fields.Clear();
				await Task.Delay(3000); //allow discord api gateway rest period
				bool isValisUser = false;
				var response = PatreonPauling.Program._PatreonHandler.GetPledges();
				PatreonPauling.Patreon.Components.Included patron;
				for (int i = 0; i < response.Included.Length; i++)
				{
					if (response.Included[i].Attributes.SocialConnections.Discord != null)
					{
						if (Convert.ToUInt64(response.Included[i].Attributes.SocialConnections.Discord.UserId) == user.Id)
						{
							isValisUser = true;
							patron = response.Included[i];

							builder.AddField("Patreon User ID:", patron.Id);
							foreach (var role in PatreonPauling.Program._PatreonConfig.Patreon.Roles)
							{
								//if payment is greaterthan or equal tp lowerbound and payment is less than or equal to upperbound
								if (response.Data[i].Attributes.AmountCents >= role.LowerPayBoundInCents && response.Data[i].Attributes.AmountCents <= role.UpperPayBoundInCents)
								{
									SocketRole socketRole = Context.Guild.Roles.FirstOrDefault(x => x.Name.ToString().ToLower() == role.DiscordRoleName);
									await user.AddRoleAsync(socketRole);
									builder.AddField("Pledge Level:", $"{role.DiscordRoleName}\n\nDiscord role applied!");
									builder.WithColor(Color.Green);
									builder.WithThumbnailUrl(user.GetAvatarUrl());
									builder.WithCurrentTimestamp();
								}
								//if upperbound is negative 1 (infinite, to cover for overpay) and payment is greater than or equal to lower bound
								if (role.UpperPayBoundInCents == -1)
								{
									if (response.Data[i].Attributes.AmountCents >= role.LowerPayBoundInCents)
									{
										SocketRole socketRole = Context.Guild.Roles.FirstOrDefault(x => x.Name.ToString().ToLower() == role.DiscordRoleName);
										await user.AddRoleAsync(socketRole);
										builder.AddField("Pledge Level:", $"{role.DiscordRoleName}\n\nDiscord role applied!");
										builder.WithColor(Color.Green);
										builder.WithThumbnailUrl(user.GetAvatarUrl());
										builder.WithCurrentTimestamp();
										await msg.Result.ModifyAsync(x =>
										{
											x.Content = "";
											x.Embed = builder.Build();
										});
									}
								}
							}
						}
						else
						{
							patron = null;
						}
					}
					else
					{
						patron = null;
					}
				}
				if (!isValisUser)
				{
					builder.AddField("Patron User ID:", "There are no patrons linked with this discord. Are your accounts linked?");
					builder.WithCurrentTimestamp();
					builder.WithThumbnailUrl(user.GetAvatarUrl());
					builder.WithColor(Color.Red);
					await msg.Result.ModifyAsync(x =>
					{
						x.Content = "";
						x.Embed = builder.Build();
					});
				}
			}
			catch (NullReferenceException e)
			{
				builder.AddField("Critical API Error:", "Patreon failed to return a valid response");
				builder.AddField("If this problem persists, please create a ticket at:", "https://bitbucket.org/RealStillkill/paulingpatreon/issues/new");
				builder.AddField("An exception has occurred:", e.StackTrace);
				builder.WithColor(Color.Red);
				builder.WithThumbnailUrl("https://discordapp.com/assets/b1868d829b37f0a81533ededb9ffe5f4.svg");
				builder.WithCurrentTimestamp();
				await msg.Result.ModifyAsync(x =>
				{
					x.Content = "";
					x.Embed = builder.Build();
				});
			}
			catch (Exception e)
			{
				builder.AddField("Critical Internal Error", "An internal exception has occurred");
				builder.AddField("If this problem persists, please create a ticket at:", "https://bitbucket.org/RealStillkill/paulingpatreon/issues/new");
				builder.AddField("An exception has occurred:", e.StackTrace);
				builder.WithColor(Color.Red);
				builder.WithThumbnailUrl("https://discordapp.com/assets/b1868d829b37f0a81533ededb9ffe5f4.svg");
				builder.WithCurrentTimestamp();
				await msg.Result.ModifyAsync(x =>
				{
					x.Content = "";
					x.Embed = builder.Build();
				});
			}

		}
	}
}