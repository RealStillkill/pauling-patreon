﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;

using Discord;
using Discord.WebSocket;
using Discord.Commands;

using Microsoft.Extensions.DependencyInjection;

using PaulingPatreon.Patreon;

namespace PatreonPauling
{
	class Program
	{
		public static DiscordSocketClient _client;
		private IServiceProvider _services;
		public static PatreonHandler _PatreonHandler;
		public static Config Config;
		public static PatreonConfig _PatreonConfig;
		public static LoggingService Logger { get; private set; }

		public static void Main(string[] args) => new Program().MainAsync().GetAwaiter().GetResult();

		private async Task MainAsync()
		{
			await Log(new LogMessage(LogSeverity.Info, "Startup", "Initializing bot..."));

			await Log(new LogMessage(LogSeverity.Info, "Startup", "Setting up service providers..."));

			Config = new Config();
			_PatreonConfig = new PatreonConfig();
			_PatreonHandler = new PatreonHandler();
			_client = new DiscordSocketClient(new DiscordSocketConfig
			{
				ConnectionTimeout = 8000,
				HandlerTimeout = 3000,
				MessageCacheSize = 25,
				LogLevel = LogSeverity.Verbose
			});

			_client.Ready += OnReady;
			_client.UserJoined += OnUserJoined;
			_services = BuildServices();
			await _services.GetRequiredService<CommandHandlingService>().InitializeAsync();
			Logger = _services.GetRequiredService<LoggingService>();

			await Log(new LogMessage(LogSeverity.Info, "Startup", "Done!"));

			await Log(new LogMessage(LogSeverity.Info, "Startup", "Logging in..."));

			await _client.LoginAsync(TokenType.Bot, Config.bot.token);
			await _client.StartAsync();

			await Task.Delay(-1);
		}

		private Task OnUserJoined(SocketGuildUser arg)
		{
			Console.WriteLine("User joined: Unhandled event");
			return null;
		}

		private async Task OnReady()
		{
			await _client.SetGameAsync("//help for commands");

			await Log(new LogMessage(LogSeverity.Info, "Startup", "Initialization complete!"));
			await Log(new LogMessage(LogSeverity.Info, "Startup", string.Format("Logged in as: {0}", _client.CurrentUser.Username)));
		}

		private Task Log(LogMessage msg) => Task.Run(() => Console.WriteLine(msg.ToString()));

		private IServiceProvider BuildServices()
			=> new ServiceCollection()
				.AddSingleton(_client)
				.AddSingleton<CommandService>()
				.AddSingleton<CommandHandlingService>()
				.AddSingleton<LoggingService>()
				.BuildServiceProvider();
	}
}